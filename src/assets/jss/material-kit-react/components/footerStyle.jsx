import { container, } from "assets/jss/material-kit-react.jsx";

const footerStyle = {
  footer: {
    backgroundColor: "#000",
    color: "#b7b7b7",
    paddingTop: "60px"
  },
  container:{
    ...container,
  },
  listHeader:{
    color: "#fff",
    fontFamily: "Poppins",
    fontSize: "20px",
    fontWeight: 600
  },
  listItem:{
    paddingTop: 0,
    paddingBottom: 0
  },
  navLink:{
    fontFamily: "Poppins",
    fontSize: "16px",
    fontWeight: 600,
    color:"#b7b7b7",
    padding: 0
  },
  paragraph:{
    marginTop: 0
  },
  copyright:{
    textAlign: "center",
    display: "inline-block",
    width: "100%"
  }
};
export default footerStyle;
