import { defaultFont, container } from "assets/jss/material-kit-react.jsx"

const heroStyle = {
  container: {
    ...container,
  },
  title: {
    ...defaultFont,
    fontSize: "62px",
    fontWeight: 800,
    textTransform: "uppercase",
    lineHeight: 1,
    color: "#000",
  },
  description: {
    ...defaultFont,
    fontSize: "20px",
    color: "#000",
  },
}

export default heroStyle
