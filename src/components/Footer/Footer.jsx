/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
import { withStyles, Grid, ListSubheader, List, ListItem, Button } from "@material-ui/core";

import footerStyle from "assets/jss/material-kit-react/components/footerStyle.jsx";

function Footer({ classes, quickLinks, address }) {

  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <Grid container>
          <Grid item xs={12} md={5}>
            
          </Grid>
          <Grid item xs>
            <ListSubheader className={classes.listHeader}>Quick links</ListSubheader>
            <List className={classes.list}>
              {quickLinks.map(link => (
                <ListItem className={classes.listItem}>
                  <Button
                    href={link.url}
                    color={link.cssClass}
                    className={`${classes.navLink}`}
                  >
                    {link.text}
                  </Button>
                </ListItem>
              ))}
            </List>
          </Grid>
          <Grid item xs>
            <ListSubheader className={classes.listHeader}>{address.title}</ListSubheader>
            <List className={classes.list}>
              <ListItem className={classes.listItem}>
                <p className={`${classes.navLink} ${classes.paragraph}`}>{address.description}</p>
              </ListItem>
            </List>
            <List className={classes.list}>
              <ListItem className={classes.listItem}>
                <p className={`${classes.navLink} ${classes.paragraph}`}>{address.phoneText}<br />{address.phoneNumber}</p>
              </ListItem>
            </List>
            <List className={classes.list}>
              <ListItem className={classes.listItem}>
                <p className={`${classes.navLink} ${classes.paragraph}`}>{address.emailText}<br />{address.email}</p>
              </ListItem>
            </List>
          </Grid>
        </Grid>
        <div className={classes.copyright}>
          <p>Copyright © 2019 Brand Name Inc. All Rights Reserved. </p>
        </div>
      </div>
    </footer>
  );
}
// Specifies the default values for props:
Footer.defaultProps = {
  address: {
    title:'',
    description:''
  },
  quickLinks: []
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(footerStyle)(Footer);
