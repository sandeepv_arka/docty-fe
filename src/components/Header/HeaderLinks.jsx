/*eslint-disable*/
import React from "react"

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"

import Button from "components/CustomButtons/Button.jsx"

import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.jsx"

function HeaderLinks({ classes, links }) {
  return (
    <List className={classes.list}>
      {links.map(link => (
        <ListItem className={classes.listItem}>
          <Button
            href={link.url}
            color={link.cssClass}
            className={`${classes.navLink}`}
          >
            {link.text}
          </Button>
        </ListItem>
      ))}
    </List>
  )
}

// Specifies the default values for props:
HeaderLinks.defaultProps = {
  links: []
};

export default withStyles(headerLinksStyle)(HeaderLinks)
