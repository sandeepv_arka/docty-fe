import React from "react"
import GridContainer from "components/Grid/GridContainer.jsx"
import GridItem from "components/Grid/GridItem.jsx"
import Parallax from "components/Parallax/Parallax.jsx"
import withStyles from "@material-ui/core/styles/withStyles"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"
// core components
import heroStyle from "assets/jss/material-kit-react/components/heroStyle"

const Hero = ({ classes, title, description, backgroundImage }) => {
  return (
    <Parallax filter image={backgroundImage.file.url}>
      <div className={classes.container}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={7}>
            <h1 className={classes.title}>{title}</h1>
            <h4 className={classes.description}>
              {documentToReactComponents(description.json)}
            </h4>
          </GridItem>
        </GridContainer>
      </div>
    </Parallax>
  )
}

Hero.propTypes = {}

export default withStyles(heroStyle)(Hero)
