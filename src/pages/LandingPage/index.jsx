import React from "react"
// nodejs library that concatenates classes
import classNames from "classnames"
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles"
import { useStaticQuery, graphql } from "gatsby"

// core components
import Header from "components/Header/Header.jsx"
import Hero from "components/Hero"
import Footer from "components/Footer/Footer.jsx"
import HeaderLinks from "components/Header/HeaderLinks.jsx"

import landingPageStyle from "assets/jss/material-kit-react/views/landingPage.jsx"

// Sections for this page
// import ProductSection from "./Sections/ProductSection.jsx"
// import TeamSection from "./Sections/TeamSection.jsx"
// import WorkSection from "./Sections/WorkSection.jsx"

const dashboardRoutes = []

const LandingPage = ({ classes, ...rest }) => {
  const data = useStaticQuery(graphql`
    {
      contentfulLandingPage {
        header {
          logo {
            title
            file {
              url
            }
          }
          megaNav {
            text
            url,
            cssClass
          }
        }
        hero {
          title
          backgroundImage {
            file {
              url
            }
          }
          description {
            json
          }
        }
        serviceCountGrid {
          title
          count
          icon {
            file {
              url
            }
          }
        }
        featureGrid1 {
          gridItem {
            image {
              file {
                url
              }
              title
            }
          }
        }
        featureGrid2 {
          gridItem {
            image {
              file {
                url
              }
              title
            }
          }
        }
        featureTile1 {
          title
          image {
            file {
              url
            }
            title
            description
          }
          shortDescription
        }
        featureTile2 {
          title
          image {
            file {
              url
            }
            title
            description
          }
          shortDescription
        }
        featureTile3 {
          title
          image {
            file {
              url
            }
            title
            description
          }
          shortDescription
        }
        footer {
          quickLinks {
            text
            url
          }
          address {
            title
            description
            phoneText
            phoneNumber
            emailText
            email
          }
        }
      }
    }
  `)
  const { header, hero, footer } = data.contentfulLandingPage
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand={header.logo}
        rightLinks={<HeaderLinks links={header.megaNav} />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <Hero {...hero}></Hero>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          {/* <ProductSection />
          <TeamSection />
          <WorkSection /> */}
        </div>
      </div>
      <Footer {...footer} />
    </div>
  )
}

export default withStyles(landingPageStyle)(LandingPage)
