import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";

import productStyle from "assets/jss/material-kit-react/views/componentsSections/productStyle.jsx";

class ProductSection extends React.Component {
  render() {
    const { classes, products } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer>
          {products.map(product => <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title={product.title}
              icon={product.icon.file.url}
            />
          </GridItem>)}
        </GridContainer>
      </div>
    );
  }
}

ProductSection.defaultProps = {
  products: []
};

export default withStyles(productStyle)(ProductSection);
