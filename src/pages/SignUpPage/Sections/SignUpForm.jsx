import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
// import MenuItem from '@material-ui/core/MenuItem';
import Button from "components/CustomButtons/Button.jsx";
import signUpFormStyle from "../../../assets/jss/material-kit-react/components/signUpFormStyle";
import Check from "@material-ui/icons/Check"
import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"

// const ranges = [
//     {
//         value: '0-20',
//         label: '0 to 20',
//     },
//     {
//         value: '21-50',
//         label: '21 to 50',
//     },
//     {
//         value: '51-100',
//         label: '51 to 100',
//     },
// ];

const SignUpFormSection = ({ classes }) => {
    // const [values, setValues] = React.useState({
    //     amount: '',
    //     password: '',
    //     weight: '',
    //     weightRange: '',
    //     showPassword: false,
    // });

    // const handleChange = prop => event => {
    //     setValues({ ...values, [prop]: event.target.value });
    // };

    // const handleClickShowPassword = () => {
    //     setValues({ ...values, showPassword: !values.showPassword });
    // };

    // const handleMouseDownPassword = event => {
    //     event.preventDefault();
    // };

    return (
        <div className={classes.container}>
            <GridContainer>
                <GridItem sm={12} md={12}>
                    <h2 className={classes.title}>Ready to Join Us? Let’s Get Started!</h2>
                    <h4 className={classes.description}>
                        How do you want to start as? Select a role below to register.
                    </h4>
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Title"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="First Name"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Last Name"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Phone"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Email"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Country"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="City"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Locality"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
                <GridItem sm={12} md={4}>
                <CustomInput
                  labelText="Pin"
                  id="name"
                  formControlProps={{
                    fullWidth: true
                  }}
                />
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem xs={12}>
                <div
                  className={
                    classes.checkboxAndRadio +
                    " " +
                    classes.checkboxAndRadioHorizontal
                  }
                >
                <FormControlLabel
                    control={
                      <Checkbox
                        tabIndex={-1}
                        onClick={() => this.handleToggle(21)}
                        checkedIcon={<Check className={classes.checkedIcon} />}
                        icon={<Check className={classes.uncheckedIcon} />}
                        classes={{ checked: classes.checked }}
                      />
                    }
                    classes={{ label: classes.label }}
                    label="Unchecked"
                  />
                  </div>
                </GridItem>
            </GridContainer>
            <GridContainer>
                <GridItem
                  xs={12}
                  sm={12}
                  md={4}
                >
                  <Button color="primary">Register Now</Button>
                </GridItem>
              </GridContainer>
        </div>
    );
}

export default withStyles(signUpFormStyle)(SignUpFormSection);
// export default SignUpFormSection;