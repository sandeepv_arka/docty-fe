import React from "react"
// nodejs library that concatenates classes
import classNames from "classnames"
// @material-ui/core components
import { withStyles } from "@material-ui/core/styles"
import { useStaticQuery, graphql } from "gatsby"

// core components
import Header from "components/Header/Header.jsx"
import Hero from "components/Hero"
import Footer from "components/Footer/Footer.jsx"
import HeaderLinks from "components/Header/HeaderLinks.jsx"

import signUpPageStyle from "assets/jss/material-kit-react/views/signUpPage.jsx"

// Sections for this page
import ProductSection from "./Sections/ProductSection"
import SignUpForm from "./Sections/SignUpForm"

const dashboardRoutes = []

const SignUpPage = ({ classes, ...rest }) => {
  const data = useStaticQuery(graphql`
    {
      contentfulSignUpPage {
        header {
          logo {
            file {
              url
            }
          }
          megaNav {
            text
            url,
            cssClass
          }
        }
        hero {
          title
          backgroundImage {
            file {
              url
            }
          }
          description {
            json
          }
        }
        featureProducts {
          icon {
            file {
              url
            }
          }
          title
        }
        footer {
          quickLinks {
            text
            url
          }
          address {
            title
            description
            phoneText
            phoneNumber
            emailText
            email
          }
        }
      }
    }
  `)
  const { header, hero, featureProducts, footer } = data.contentfulSignUpPage
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand={header.logo}
        rightLinks={<HeaderLinks links={header.megaNav} />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <Hero {...hero}></Hero>
      <div className={classNames(classes.main, classes.mainRaised)}>
          <ProductSection products={featureProducts}/>
      </div>
      <SignUpForm />
      <Footer {...footer} />
    </div>
  )
}

export default withStyles(signUpPageStyle)(SignUpPage)
