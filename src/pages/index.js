import React from "react";
import {createMemoryHistory} from "history";
import {Route, Router, Switch} from "react-router-dom";
//import { createMuiTheme } from '@material-ui/core/styles';
// import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import 'typeface-poppins';
// import customTheme from '../theme';
// pages for this product
import LandingPage from "./LandingPage"
import ProfilePage from "./ProfilePage/ProfilePage.jsx";
import LoginPage from "./LoginPage/LoginPage.jsx";
import SignUpPage from "./SignUpPage";

let hist = createMemoryHistory();

// const theme = createMuiTheme(customTheme);
export default () => (
  // <ThemeProvider theme={theme}>
  <Router history={hist}>
      <CssBaseline></CssBaseline>
    <Switch>
      <Route path="/" component={LandingPage} />
      <Route path="/SignUpPage" component={SignUpPage} />
      <Route path="/profile-page" component={ProfilePage} />
      <Route path="/login-page" component={LoginPage} />
    </Switch>
  </Router>
  // </ThemeProvider>
);
