import React from "react";
import { graphql } from "gatsby"
import LoginPage from "./LoginPage/LoginPage";

export const LandingPageQuery = graphql`
  {
    contentfulLandingPage {
      header {
        logo {
          file {
            url
          }
        }
        megaNav {
          text
          url
        }
      }
    }
  }
`

export default ({data}) => (
  <LoginPage data={data}/>
);
